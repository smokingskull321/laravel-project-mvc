<html>
    <head>
        <h1> Buat Account Baru! </h1>
    </head>
    <body>
        <h3> Sign Up Form </h3>
        <form action="welcome">
            <label> First Name : </label><br>
                <input type="text" placeholder="Nama Depan"><br><br>
            <label> Last Name : </label><br>
                 <input type="text" placeholder="Nama Belakang"><br><br>
            <label> Gender : </label> <br>
            <input type="radio" name="gender" value="1"> Laki-laki <br>
            <input type="radio" name="gender" value="2"> Perempuan <br>
            <input type="radio" name="gender" value="3"> Other <br>
            <br>
            <label> Nationality : </label> <br>
            <select>
                <option> Arabian </option>
                <option> England </option>
                <option> France </option>
                <option> Indonesian </option>
                <option> United States America </option>
            </select> <br><br>
            <label> Language Spoken : </label> <br>
            <input type="checkbox" name="language" value="1"> Bahasa Indonesia <br>
            <input type="checkbox" name="language" value="2"> English <br>
            <input type="checkbox" name="language" value="3"> Other <br>
            <br>
            <label> Bio : </label><br>
            <textarea cols="30" rows="10">  </textarea>
            <br>
            <input type="submit" value="Sign Up">
        </form>

    </body>
</html>